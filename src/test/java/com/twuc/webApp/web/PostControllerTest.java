package com.twuc.webApp.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.domain.Post;
import com.twuc.webApp.domain.PostRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class PostControllerTest extends ApiTestBase {
    @Autowired
    private PostRepository repository;

    private List<Long> createPosts(List<Post> posts) {
        List<Long> ids = new ArrayList<>();

        clear(em -> {
            repository.saveAll(posts);
            repository.flush();
            ids.addAll(posts.stream().map(Post::getId).collect(Collectors.toList()));
        });

        return ids;
    }

    @Test
    void should_get_all_posts() throws Exception {
        createPosts(Arrays.asList(new Post("post1", "content1"), new Post("post2", "content2")));

        String json = getMockMvc().perform(get("/api/posts"))
            .andExpect(status().is(200))
            .andReturn().getResponse().getContentAsString();

        PostResponseForTest[] posts = new ObjectMapper().readValue(json, PostResponseForTest[].class);

        assertEquals(2, posts.length);
        assertEquals(1, posts[0].getId());
        assertEquals("post1", posts[0].getTitle());
        assertEquals("content1", posts[0].getDescription());
        assertEquals(2, posts[1].getId());
        assertEquals("post2", posts[1].getTitle());
        assertEquals("content2", posts[1].getDescription());
    }

    @Test
    void should_get_zero_posts() throws Exception {
        String json = getMockMvc().perform(get("/api/posts"))
            .andExpect(status().is(200))
            .andReturn().getResponse().getContentAsString();

        PostResponseForTest[] posts = new ObjectMapper().readValue(json, PostResponseForTest[].class);

        assertEquals(0, posts.length);
    }

    @Test
    void should_create_post() throws Exception {
        getMockMvc().perform(post("/api/posts")
            .contentType(MediaType.APPLICATION_JSON_UTF8)
            .content(new ObjectMapper().writeValueAsString(new CreatePostRequestForTest("title", "content"))))
            .andExpect(status().is(201));

        List<Post> posts = repository.findAll();

        assertEquals(1, posts.size());
        assertEquals("title", posts.get(0).getTitle());
        assertEquals("content", posts.get(0).getContent());
    }

    @Test
    void should_remove_post() throws Exception {
        List<Long> ids = createPosts(
            Arrays.asList(
                new Post("post1", "content1"),
                new Post("post2", "content2")));

        getMockMvc().perform(delete("/api/posts/" + ids.get(1)))
            .andExpect(status().is(200));

        List<Post> posts = repository.findAll();
        assertEquals(1, posts.size());
        assertEquals(ids.get(0), posts.get(0).getId());
    }

    @Test
    void should_return_success_when_deleting_not_exist_post() throws Exception {
        int notExistId = 2333;
        getMockMvc().perform(delete("/api/posts/" + notExistId))
            .andExpect(status().is(200));
    }
}

class CreatePostRequestForTest {
    private final String title;
    private final String description;

    public CreatePostRequestForTest(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }
}

class PostResponseForTest {
    private long id;
    private String title;
    private String description;

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }
}